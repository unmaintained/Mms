source = icon.svg
output = ic_launcher_smsmms.png
mipmap = res/mipmap
densities = mdpi hdpi xhdpi xxhdpi xxxhdpi

all : $(foreach x, $(densities), $(mipmap)-$(x)/$(output))

$(mipmap)-mdpi/$(output) : $(source)
	inkscape $< -e $@ -w 48

$(mipmap)-hdpi/$(output) : $(source)
	inkscape $< -e $@ -w 72

$(mipmap)-xhdpi/$(output) : $(source)
	inkscape $< -e $@ -w 96

$(mipmap)-xxhdpi/$(output) : $(source)
	inkscape $< -e $@ -w 144

$(mipmap)-xxxhdpi/$(output) : $(source)
	inkscape $< -e $@ -w 192

.PHONY : all
